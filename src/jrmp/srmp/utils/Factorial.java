/**
 * 
 */
package jrmp.srmp.utils;

/**
 * @author micro
 *
 */
public class Factorial {

	public static int fac(int x) {
		
        if (x < 0) {
            throw new IllegalArgumentException("x must be>=0");
        }
        if (x <= 1) {
            return 1;
        } else
            return x * fac(x - 1);
    }

}
