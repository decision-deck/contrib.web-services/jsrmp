package jrmp.srmp.main;

import java.io.IOException;

import jrmp.srmp.base.SRMPaggregator;
import jrmp.srmp.settings.Config;

import org.apache.xmlbeans.XmlException;
import org.decisiondeck.jmcda.exc.InvalidInputException;

public class TestAggr extends Accessible {

	public static void main(String[] args) throws IOException, XmlException, InvalidInputException {
		
		Config.RESOURCES_FOLDER = "";
		
		prepare(args);
		
		/////////////////
		////  BEGIN  ////
		/////////////////
		
		reader.read(false,"a");
		
		SRMPaggregator aggr = new SRMPaggregator(reader);
		aggr.execute(false);
		
		writer.copy(aggr.getOutput());
		writer.writeRanking(true);
		
		///////////////
		////  END  ////
		///////////////
		
		end();

	}

}
